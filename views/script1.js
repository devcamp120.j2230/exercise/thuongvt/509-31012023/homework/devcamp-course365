/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 14,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        }
    ]
}
// khai báo mọt biến luu trư Id của đối tượng sửa hoặc xoá
var gIdchoose = 0;
// khai báo một biến lưu trữ đối tượng được tìm ra từ mảng
var gOject ="";
//B0 khai báo một biến chứa dữ liệu khai báo
var gNewCouse = {
    id: 0,
    courseCode: "",
    courseName: "",
    price: 0,
    duration: "",
    level: "",
    coverImage: "",
    teacherName: "",
    teacherPhoto: "",
    isPopular: -1,
    isTrending: -1
}

$(document).ready(function () {
    var vDataCol = ["STT","id", "courseCode", "courseName", "price", "duration", "level", "teacherName","isPopular","isTrending","action"] // Gán với tên các cột trong array
    const gSTT = 0;
    const gId = 1;
    const gCourseCode = 2;
    const gCourseName = 3;
    const gPrice = 4;
    const gDuration = 5;
    const gLevel = 6;
    const gTeacherName = 7;
    const gIsPopular = 8;
    const gIsTrending = 9;
    const gAction = 10;
    var gstt = 0;

   var gTable = $("#user-table").DataTable({
      columns: [
        { "data": vDataCol[gSTT] },
        { "data": vDataCol[gId] },
        { "data": vDataCol[gCourseCode] },
        { "data": vDataCol[gCourseName] },
        { "data": vDataCol[gPrice] },
        { "data": vDataCol[gDuration] },
        { "data": vDataCol[gLevel] },
        { "data": vDataCol[gTeacherName] },
        { "data": vDataCol[gIsPopular] },
        { "data": vDataCol[gIsTrending] },
        { "data": vDataCol[gAction] },
      ],
      columnDefs: [
        {
          targets: gAction,
          defaultContent: `
        <button class="btn btn-link btn-edit" data-toggle="tooltip" data-placement="bottom" title="Dice History"><i class="fas fa-dice text-success"></i></button>
        <button class="btn btn-link btn-sm" data-toggle="tooltip" data-placement="bottom" title="Voucher History"><i class="fas fa-gift text-success"></i></button>`
        },
        {
          targets: gSTT,
          render: function (Data) {
            gstt++;
            return gstt;
          }
        }
      ]
    })
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
// lọc dữ liệu và đổ vào most popular
//filterMostPopular();
// lọc dữ liệu và đổ vào trending
//filterTrending();
//load dữ liệu vào bảng
loadDataToFrom(gCoursesDB.courses);
//Thêm một khoá học mới
$(document).on("click", "#id-add", function(){
    addBtnCuose();
})
// Hàm xử lý sự kiện khi ấn nút add couse
$(document).on("click", "#btn-modal-add", function(){
    addBtnModalAdd();
})
// Hàm xử lý sự kiện khi ấn nút edit couse
$("#user-table").on("click", ".btn-edit", function(){
    onBtnEditClick(this)
})
// hàm xử lý sự kiện khi ấn nút Edit couse
$(document).on("click","#btn-modal-edit",function(){
    onBtnModalEditClick(this)
})
// hàm xử lý sự kiện khi ấn nút delete couse
$("#user-table").on("click", ".btn-sm", function(){
    onBtnDeleteClick(this)
})
// gán sự kiện cho nút Confirm Delete Voucher (trên modal)
$("#btn-confirm-delete-voucher").on("click", function() {
    onBtnConfirmDeleteVoucherClick();
  });

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  
// hàm load dữ liệu
function loadDataToFrom(paramData) {
    var vTable = $("#user-table").DataTable()
    vTable.clear();
    vTable.rows.add(paramData);
    vTable.draw();
  }
// Hàm hiện modal  thêm một khoá học mới
function addBtnCuose(){
    $("#user-modal").modal('show');
}
// hàm xử lý sự kiện thêm khoá học mới
function addBtnModalAdd(){
    console.log("hàm đã được gọi");
    //B1 Thu thập dữ liệu 
    getDataNewCouse(gNewCouse);
    //B2 kiểm tra dữ liệu
    var vCheck = ValidateNewCouse(gNewCouse);
    console.log(gNewCouse);
    //console.log(vCheck);
    if(vCheck){
    //B3 Thêm mới dữ liệu vào bảng đã có sẵn
    insertNewCouse(gNewCouse);
    console.log(gCoursesDB.courses);
    // B4: xử lý front-end
    console.log("Thêm couse thành công!");
    loadDataToNewCuoseTable(gCoursesDB.courses);
    // xoá trắng dữ liệu trên form
    resertaddcouseForm()
    $("#user-modal").modal('hide');
    }
}
// hàm thu thập dữ liệu khoá học mới
function getDataNewCouse(paramData){
    paramData.id= getNextId();
    paramData.courseCode = $("#courseCode").val().trim();
    paramData.courseName = $("#courseName").val().trim();
    paramData.price =Number($("#price").val().trim()) ;
    paramData.duration = $("#duration").val().trim();
    paramData.level = $("#level").val().trim();
    paramData.coverImage = $("#coverImage").val().trim();
    paramData.teacherName = $("#coverImage").val().trim();
    paramData.teacherPhoto = $("#teacherPhoto").val().trim();
    paramData.isPopular =  getBlooeanValueIsPopular();
    paramData.isTrending = getBlooeanValueIsTrending();
}
// hàm trả về giá trị boolean
function getBlooeanValueIsPopular(){
    var vValueIsPopular = $("#isPopular").val();
    if(vValueIsPopular=="true"){
        vValueIsPopular = true;
    }
    else {
        vValueIsPopular = false;
    }
    return vValueIsPopular
}
// hàm trả về giá trị boolean
function getBlooeanValueIsTrending(){
    var vValueIsTrending = $("#isTrending").val();
    if(vValueIsTrending=="true"){
         vValueIsTrending = true;
    }
    else {
     vValueIsTrending = false;
    }
    return vValueIsTrending
}
// hàm kiểm tra dữ liệu nhập vào
function ValidateNewCouse(paramData){
    if(paramData.courseCode==""){
        console.log("dữ liệu courseCode chưa nhập");
        return false;
    }
    if(paramData.courseName==""){
        console.log("dữ liệu courseName chưa nhập");
        return false;
    }
    if(isNaN(paramData.price)==true){
        console.log("dữ liệu nhập vào phải là số");
        return false;
    }
    if(paramData.duration==""){
        console.log("dữ liệu duration chưa nhập");
        return false;
    }
    if(paramData.level=="Not-select"){
        console.log("dữ liệu level chưa nhập");
        return false;
    }
    if(paramData.coverImage==""){
        console.log("dữ liệu coverImage chưa nhập");
        return false;
    }
    if(paramData.teacherName==""){
        console.log("dữ liệu teacherName chưa nhập");
        return false;
    }
    if(paramData.teacherPhoto==""){
        console.log("dữ liệu teacherPhoto chưa nhập");
        return false;
    }
    if(paramData.isPopular=="Not-select"){
        console.log("dữ liệu isPopular chưa nhập");
        return false;
    }
    if(paramData.isTrending=="Not-select"){
        console.log("dữ liệu isTrending chưa nhập");
        return false;
    }
    return true;
}
 // hàm lấy ra đc id voucher tiếp theo, dùng khi thêm mới voucher
 function getNextId() {
    var vNextId = 0;
    // nếu mảng chưa có phần tử nào, thì id sẽ bắt đầu từ 1
    if(gCoursesDB.courses.length == 0) {
      vNextId = 1;
    }
    else { // id tiếp theo bằng id của phần tử cuối cùng cộng thêm 1
      vNextId = gCoursesDB.courses[gCoursesDB.courses.length- 1].id + 1;
    }
    return vNextId;
  }
  // hàm thêm mới dữ liệu vào bảng
  function insertNewCouse(paramData){
    gCoursesDB.courses.push(gNewCouse);
  }
  // Hàm load data to table
  function loadDataToNewCuoseTable(paramData){
    gstt= 1;
    var vTable = $("#user-table").DataTable()
    vTable.clear();
    vTable.rows.add(paramData);
    vTable.draw();
  }
//  // hàm xóa trắng form add new couse
  function resertaddcouseForm() {
    $("#courseCode").val("");
    $("#courseName").val("");
    $("#price").val("");
    $("#duration").val("");
    $("#level").val("Not-select");
    $("#coverImage").val("");
    $("#coverImage").val("");
    $("#teacherPhoto").val("");
    $("#isPopular").val("Not-select");
    $("#isTrending").val("Not-select");
  } 
  // hàm khi ấn nút edit
  function onBtnEditClick(paramData){
    // lấy Id của mảng đƯợc chọn
    gIdchoose = getIdForm(paramData);
    // Hiện thông tin mảng đƯợc chọn lên Modal
    showVoucherDataToModal(gIdchoose);
    $("#user-modal-edit").modal('show');
    //console.log(gIdchoose);
  }
  // hàm lấy Id
  function getIdForm(paramButton){
    var vTableRow = $(paramButton).parents("tr");
    var vVoucherRowData = gTable.row(vTableRow).data();
    return vVoucherRowData.id;
  }
  //Hàm show Data lên mảng đƯợc chọn
  function showVoucherDataToModal(paramData){
    var vCouseIndex = getIndexFormCouse(paramData);
    console.log(vCouseIndex);
    $("#id-courseCode").val(gCoursesDB.courses[vCouseIndex].courseCode);
    $("#idcourseName").val(gCoursesDB.courses[vCouseIndex].courseName);
    $("#idprice").val(gCoursesDB.courses[vCouseIndex].price);
    $("#idduration").val(gCoursesDB.courses[vCouseIndex].duration);
    $("#idlevel").val(gCoursesDB.courses[vCouseIndex].level);
    $("#idcoverImage").val(gCoursesDB.courses[vCouseIndex].coverImage);
    $("#idteacherName").val(gCoursesDB.courses[vCouseIndex].teacherName);
    $("#idteacherPhoto").val(gCoursesDB.courses[vCouseIndex].teacherPhoto);
    $("#idisPopular").val(gCoursesDB.courses[vCouseIndex].isPopular);
    $("#idisTrending").val(gCoursesDB.courses[vCouseIndex].isTrending);
  }

  
  // hàm khi ấN nút edit modal
  function onBtnModalEditClick(paramData){
    console.log("nút đã được bấm")
    // tao một biến luu trữ đối tượng 
    var vEditCouse = {
        id:"",
        courseCode: "",
        courseName: "",
        price: 0,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular:-1,
        isTrending:-1
    }
    
    // B1: Thu thập dữ liệu
    getEditData(vEditCouse);
    console.log(vEditCouse);
    // B2 update data
    updateData(vEditCouse);
    // load dữ liệu mới sửa ra bảng
    loadDataToFromCourse(gCoursesDB.courses);
    // xoá trắng dữ liệu trên modal
    resertEditcouseForm()
    // ẩn modal
    $("#user-modal-edit").modal('hide');



  }
  // hàm thu thậP dữ liệU Edit
  function getEditData(paramEditData){
    paramEditData.id = gIdchoose;
    paramEditData.courseCode = $("#id-courseCode").val();
    paramEditData.courseName = $("#idcourseName").val();
    paramEditData.price = Number($("#idprice").val()) ;
    paramEditData.duration = $("#idduration").val();
    paramEditData.level = $("#idlevel").val();
    paramEditData.coverImage = $("#idcoverImage").val();
    paramEditData.teacherName = $("#idteacherName").val();
    paramEditData.teacherPhoto = $("#idteacherPhoto").val();
    paramEditData.isPopular =  getBlooeanValueidIsPopular();
    paramEditData.isTrending = getBlooeanValueidIsTrending();
  }
// hàm trả về giá trị boolean
function getBlooeanValueidIsPopular(){
    var vValueIsPopular = $("#idisPopular").val();
    if(vValueIsPopular=="true"){
        vValueIsPopular = true;
    }
    else {
        vValueIsPopular = false;
    }
    return vValueIsPopular
}
// hàm trả về giá trị boolean
function getBlooeanValueidIsTrending(){
    var vValueIsTrending = $("#idisTrending").val();
    if(vValueIsTrending=="true"){
         vValueIsTrending = true;
    }
    else {
     vValueIsTrending = false;
    }
    return vValueIsTrending
}
// hàm thực hiện update data vào mảng
   function updateData(paramVoucherObj) {
    var vVoucherIndex = getIndexFormCouse(gIdchoose);
   gCoursesDB.courses.splice(vVoucherIndex, 1, paramVoucherObj);
   console.log(gCoursesDB.courses);
  }

// Xoá trắng dữ liệu trên modal
function resertEditcouseForm(){
    $("#id-courseCode").val("");
    $("#idcourseName").val("");
    $("#idprice").val("");
    $("#idduration").val("");
    $("#idlevel").val("Not-select");
    $("#idcoverImage").val("");
    $("#idteacherName").val("");
    $("#idteacherPhoto").val("");
    $("#idisPopular").val("Not-select");
    $("#idisTrending").val("Not-select");
}
// hàm khi ấn nút delete
function onBtnDeleteClick(paramData){
   // console.log("hàm delêt được ấn")
   // lấy Id của mảng đƯợc chọn
   gIdchoose = getIdForm(paramData);
   // Hiện thông tin mảng đƯợc chọn lên Modal
   $("#delete-confirm-modal").modal('show');
}
 // hàm xử lý sự kiện confirm delete voucher modal click
 function onBtnConfirmDeleteVoucherClick() { 
    // B1: Thu thập dữ liệu (ko cần)
    // B2: Validate update
    // B3: delete voucher
    deleteVoucher(gIdchoose);
    // B4: xử lý front-end
    //alert("Xóa voucher thành công!");
    loadDataToFromCourse(gCoursesDB.courses);
    $("#delete-confirm-modal").modal("hide");
  }
  // hàm xóa voucher theo id voucher
  function deleteVoucher(paramVoucherId) {
    var vVoucherIndex = getIndexFormCouse(paramVoucherId);
    gCoursesDB.courses.splice(vVoucherIndex, 1);
    console.log(gCoursesDB.courses)
  }

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm lấy chỉ số của đỐi tượng được chọn
function getIndexFormCouse(paramDataId){
    var vCouseIndex = -1;
    var vCouseFound = false;
    var vLoopIndex = 0;
    while(!vCouseFound && vLoopIndex < gCoursesDB.courses.length) {
      if(gCoursesDB.courses[vLoopIndex].id === paramDataId) {
        vCouseIndex = vLoopIndex;
        vCouseFound = true;
      }
      else {
        vLoopIndex ++;
      }
    }
    return vCouseIndex;
  }
  // hàm load dữ liệu sửa courser ra bảng
function loadDataToFromCourse(paramData){
    gstt= 0;
    var vTable = $("#user-table").DataTable()
    vTable.clear();
    vTable.rows.add(paramData);
    vTable.draw();
}
})


