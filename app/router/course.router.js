//Khai báo thư viện express
const express = require("express");
// Khai báo Middleware
const CRUDMiddleware = require("../middleware/course.middleware");
//khai báo controller
const {
    createCourse,
    getAllCourse,
    getCourseById,
    updateCourseById,
    deleteCourseById
} = require("../controller/course.controller")

// Tạo router
const CourseRouter = express.Router();
// Sử dụng router
CourseRouter.post("/Courses",CRUDMiddleware.CRUDMiddleware,createCourse);
CourseRouter.get("/Courses",CRUDMiddleware.CRUDMiddleware,getAllCourse);
CourseRouter.get("/Courses/:CourseId",CRUDMiddleware.CRUDMiddleware,getCourseById);
CourseRouter.put("/Courses/:CourseId",CRUDMiddleware.CRUDMiddleware,updateCourseById);
CourseRouter.delete("/Courses/:CourseId",CRUDMiddleware.CRUDMiddleware,deleteCourseById)





//exports router
module.exports = {
    CourseRouter
}