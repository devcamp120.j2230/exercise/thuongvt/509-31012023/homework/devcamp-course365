// khai báo thư viện mongoose
const mongoose = require("mongoose");
// Khai báo model
const courseModel = require("../model/course.model");

//Tạo một course mới
const createCourse = (req, res) => {
    //B1 Chuẩn bị dữ liệu
    let body = req.body;
    // B2 Kiểm tra dữ liệu đầu vào chia nhỏ các trường hợp để kiểm tra.
    if (!body.courseCode) {
        return res.status(400).json({
            message: 'dữ liệu courseCode không đúng'
        })
    }
    if (!body.courseName) {
        return res.status(400).json({
            message: 'dữ liệu courseName không đúng'
        })
    }
    if (!Number.isInteger(body.price) || body.price < 0) {
        return res.status(400).json({
            message: 'dữ liệu price không đúng'
        })
    }
    if (!body.duration) {
        return res.status(400).json({
            message: 'dữ liệu duration không đúng'
        })
    }
    if (!body.level) {
        return res.status(400).json({
            message: 'dữ liệu level không đúng'
        })
    }
    if (!body.coverImage) {
        return res.status(400).json({
            message: 'dữ liệu coverImage không đúng'
        })
    }
    if (!body.teacherName) {
        return res.status(400).json({
            message: 'dữ liệu teacherName không đúng'
        })
    }
    if (!body.teacherPhoto) {
        return res.status(400).json({
            message: 'dữ liệu teacherPhoto không đúng'
        })
    }
    if (typeof body.isPopular != "boolean") {
        return res.status(400).json({
            message: 'dữ liệu isPopular không đúng'
        })
    }
    if (typeof body.isTrending != "boolean") {
        return res.status(400).json({
            message: 'dữ liệu isTrending không đúng'
        })
    }
    //B3 gọi model thực hiện tạo mới dữ liệu
    // Khai báo một biến hứng dữ liệu được khai báo trong req.body
    let newCourse = {
        _id: mongoose.Types.ObjectId(),
        courseCode: body.courseCode,
        courseName: body.courseName,
        price: body.price,
        discountPrice: body.discountPrice,
        duration: body.duration,
        level: body.level,
        coverImage: body.coverImage,
        teacherName: body.teacherName,
        teacherPhoto: body.teacherPhoto,
        isPopular: body.isPopular,
        isTrending: body.isTrending
    }
    // thực hiện tạo mới
    courseModel.create(newCourse, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: err
            })
        }
        else {
            return res.status(201).json({
                message: "Tạo mới thành công",
                Course: data
            })
        }
    })
}

//Tạo function getAllCourse
const getAllCourse = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    //B2 Kiểm tra dữ liệu
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    courseModel.find((err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(201).json({
                 message:"Tải toàn bộ dữ liệu thành công",
                 Course: data
             })
         }
    })
}

// Tạo function getCourseById
const getCourseById =(req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id = req.params.CourseId
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
          message: 'id không hợp lệ'
        })
      }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    courseModel.findById(id,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err
             })
         }
         else{
             return res.status(201).json({
                 message:"Tải thành công thông qua id",
                 Course: data
             })
         }
    })
}
// Tạo function updateCourseById
const updateCourseById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id = req.params.CourseId
    let body = req.body;
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
          message: 'id không hợp lệ'
        })
    }
    if(!body.courseCode) {
        return res.status(400).json({
          message: 'dữ liệu courseCode không đúng'
        })
    }
    if(!body.courseName) {
        return res.status(400).json({
          message: 'dữ liệu courseName không đúng'
        })
    }
    if(!Number.isInteger(body.price)|| body.price < 0) {
        return res.status(400).json({
          message: 'dữ liệu price không đúng'
        })
    }
    if(!Number.isInteger(body.discountPrice)|| body.discountPrice < 0) {
        return res.status(400).json({
          message: 'dữ liệu discountPrice không đúng'
        })
    }
    if(!body.duration) {
        return res.status(400).json({
          message: 'dữ liệu duration không đúng'
        })
    }
    if(!body.level) {
        return res.status(400).json({
          message: 'dữ liệu level không đúng'
        })
    }
    if(!body.coverImage) {
        return res.status(400).json({
          message: 'dữ liệu coverImage không đúng'
        })
    }
    if(!body.teacherName) {
        return res.status(400).json({
          message: 'dữ liệu teacherName không đúng'
        })
    }
    if(!body.teacherPhoto) {
        return res.status(400).json({
          message: 'dữ liệu teacherPhoto không đúng'
        })
    }
    if(typeof body.isPopular != "boolean") {
        return res.status(400).json({
          message: 'dữ liệu isPopular không đúng'
        })
    }
    if(typeof body.isTrending != "boolean") {
        return res.status(400).json({
          message: 'dữ liệu isTrending không đúng'
        })
    }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    // chuẩn bị dữ liệu
    let updateCourse = {
        courseCode:body.courseCode,
        courseName:body.courseName,
        price:body.price,
        discountPrice:body.discountPrice,
        duration:body.duration,
        level:body.level,
        coverImage:body.coverImage,
        teacherName:body.teacherName,
        teacherPhoto:body.teacherPhoto,
        isPopular: body.isPopular,
        isTrending: body.isTrending
    }
    // gọi hàm thực hiện chỉnh sửa
    courseModel.findByIdAndUpdate(id,updateCourse,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err
             })
         }
         else{
             return res.status(201).json({
                 message:"sửa thành công thông qua id",
                 Course: data
             })
         }
    })
}
// Tạo function deleteCourseById
const deleteCourseById =(req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id = req.params.CourseId
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
          message: 'id không hợp lệ'
        })
    }
    //B3 Gọi model thực hiện các thao tác nghiệp vụ
    courseModel.findByIdAndDelete(id,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(201).json({
                 message:"xóa thành công thông qua id",
                 Course: data
             })
         }
    })
}



//Export thư viện controller thành một modul
module.exports = {
    createCourse,
    getAllCourse,
    getCourseById,
    updateCourseById,
    deleteCourseById
}